# MCC Manual

This is a manual for free and open-source powder pattern indexing program
[MCC](https://gitlab.com/mcc-crystal/mcc). It is designed for users of the
program. Since MCC is a very configurable program, it provides many details
about its components, as well as operation instructions and quick reference.

To obtain the documentation, download the file [`mcc-manual.pdf`](https://gitlab.com/mcc-crystal/mcc-manual/blob/master/mcc-manual.pdf).

If you find the manual difficult to use, please provide feedback stating what
is unclear or missing so it can be improved. This can be done by opening an
[issue](https://gitlab.com/mcc-crystal/mcc-manual/issues) on the project page.

Visit the [MCC group](https://gitlab.com/mcc-crystal) for MCC source code and
pre-compiled binaries.