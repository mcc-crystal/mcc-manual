\chapter{Installation}

MCC consists of only one executable file (for example \texttt{mcc.exe}), and is therefore easy to install. There are two installation options:
\begin{itemize}
  \item Place the file in a folder along with input data and run MCC from this folder.
  \item Place MCC in its own folder and add it to system path (consult web resources on how to add a directory to system path on your platform). MCC can then be run from any directory.
\end{itemize}



\section{Using a pre-compiled binary}

Pre-compiled binaries are the easiest way to get MCC running. They can be obtained from \url{https://gitlab.com/mcc-crystal/mcc-builds}. Note that the executable files provided may not be available for all platforms and are not optimized for user's native CPU architecture.



\section{Building from source code}

By compiling MCC from its source code it is possible to obtain executable files for any platform with a compiler that supports C++ standard C++11 or higher. Additionaly, it is possible to optimize the executable for user's native CPU architecture, increasing performance.


\subsection{Requirements}

To compile MCC from source the following free tools are necessary:
\begin{itemize}
  \item C++ compiler supporting standard C++11 or higher. Generally, GCC~\cite{gnu-gcc} is recommended. For Windows, TDM-GCC~\cite{tdm-gcc} is recommended.
  \item CMake~\cite{cmake} is required to prepare build tree prior to build process.
  \item Build system like GNU Make~\cite{gnu-make} or Ninja~\cite{ninja-build} to perform the build process. In theory, any build system that CMake can generate build tree for could be used. See~\cite{cmake-generators} for a list of generators CMake supports.
  \item Optionally, Boost C++ libraries~\cite{boost} are required to compile and run unit tests.
\end{itemize}


\subsection{Build process}

\subsubsection*{Obtaining source code}

Obtain MCC source code from \url{https://gitlab.com/mcc-crystal/mcc}. Optionally, Git~\cite{git} may be used to clone the repository. Move into the root folder, which contains subfolders \texttt{src} and \texttt{tests}, and file \texttt{CMakeLists.txt}. The root folder will be reffered as \texttt{/} from here on.

\subsubsection*{Generating build tree: console way}

Create a folder named \texttt{build} inside the root folder of the source code and move into it. Execute the following command (do not forget the dots at the end; they signify that the root \texttt{CMakeLists.txt} resides in the parent directory):
\begin{lstlisting}
  $ cmake -DCMAKE_BUILD_TYPE=Release -G "Ninja" ..
\end{lstlisting}

Replace \texttt{"Ninja"} after flag \texttt{-G} with another build system, for example \texttt{"Unix Makefiles"}, if you do not use Ninja build system (see~\cite{cmake-generators} for full list of generators). This should configure the build tree to produce 64-bit code optimized for speed and current CPU architecture, and generate necessary files for chosen build system.

Instead of \texttt{cmake} it is also possible to use \texttt{ccmake}, which provides a console interface to change build options (list of manually added build options is displayed in table~\ref{tab:build-options}). It can be run with the following command:
\begin{lstlisting}
  $ ccmake -DCMAKE_BUILD_TYPE=Release -G "Ninja" ..
\end{lstlisting}

\noindent Follow on-screen instructions to change options and generate build tree.

\begin{table}[h]
  \centering
  \caption{Options for build tree generation. All options and enabled by default.}
  \begin{tabular}{ l p{0.6\textwidth} }
    \textbf{Option} & \textbf{Description} \\
    \hline
    \texttt{FORCE\_64}       & Force 64-bit build. Use this on 64-bit operating systems. If, for some reason, compiler does not support 64-bit builds, warning will be displayed. \\
    \texttt{NATIVE\_BUILD}   & Build for current CPU architecture. Use this if the executable is only intended to be used on the current PC. \\
  \end{tabular}
  \label{tab:build-options}
\end{table}

\subsubsection*{Generating build tree: graphical way}

Especially on Windows, use of graphical user interface for CMake may be desired. In field \enquote{Where is the source code} browse to the root folder containing subfolders \texttt{src} and \texttt{tests}, and file \texttt{CMakeLists.txt}. In field \enquote{Where to build the binaries} browse to folder \texttt{build} created before. Click Configure. In the dialog that appears, specify the desired build system. On Windows it may also be necessary to manually specify the compilers. When done, set \texttt{CMAKE\_BUILD\_TYPE} to \texttt{Release} and change other options from table~\ref{tab:build-options} if desired. Finally, click Generate to prepare the build tree.

\subsubsection*{Building MCC}

After generating the build tree, simply run chosen build system from \texttt{/build} and wait for MCC to build. In case of Ninja, this is achieved with a simple command:
\begin{lstlisting}
  $ ninja
\end{lstlisting}

\noindent In case of Unix Makefiles (GNU Make), the command is:
\begin{lstlisting}
  $ make
\end{lstlisting}

Finally, find MCC executable \texttt{mcc} or \texttt{mcc.exe} in folder \texttt{/build/src}. If unit test target was compiled as well, it is located in folder \texttt{/build/tests}. Executable name should be \texttt{mcc\_tests} or \texttt{mcc\_tests.exe}.