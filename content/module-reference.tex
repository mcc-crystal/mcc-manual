\chapter{Module reference}
\label{ch:module-reference}

This chapter lists all module implementations available for use, grouped by general module names. Which module implementation is used is determined by settings.



\section{Calculated peak list}

\subsection{DynamicCalculatedList}

\texttt{DynamicCalculatedList} uses dynamic array to store calculated reflections (full reflection data including Miller indices and interplanar spacing $d$). It requires no special configuration. It can be used by setting \texttt{[Modules] calculatedList} to \texttt{"Dynamic"}.



\section{Cell evaluator}

\subsection{SmithSnyderEvaluator}
\label{sec:smith-snyder-evaluator}

\texttt{SmithSnyderEvaluator} evaluates unit cells by calculating Smith-Snyder figure of merit $F_N$~\cite{smith-snyder} shown in equation~\ref{eq:smith-snyder}. $N$ represents number of observed peaks, $N_{possible}$ number of possible calculated peaks for given unit cell, and $|\overline{\Delta 2\Theta}|$ is the average absolute difference between observed peak and the nearest calculated peak, calculated as shown in equation~\ref{eq:smith-snyder-average}~\cite{fundamentals-pxrd}.

\begin{equation}
  F_N = \frac{N}{N_{possible}} \frac{1}{|\overline{\Delta 2\Theta}|}
  \label{eq:smith-snyder}
\end{equation}

\begin{equation}
  |\overline{\Delta 2\Theta}| = \frac{1}{N} \sum_{i = 1}^{N}|2\Theta_i^{obs} - 2\Theta_i^{calc}|
  \label{eq:smith-snyder-average}
\end{equation}

Its only input data are lists of calculated and observed peaks. The higher $F_N$, the better the unit cell describes observed peaks. It is a child module of some energy calculators and is loaded automatically when needed.



\section{Cell stats}
\label{sec:module-cellstats}

\texttt{CellStats} module has only one implemenation. Its purpose is to provide a centralized facility to determine number of indexed, unindexed and possible peaks by comparing lists of calculated and observed peaks.

For determination of indexed and unindexed peaks every observed peak is compared to the nearest calculated peak. If absolute difference between their \ttheta\ positions (in degrees) is less than or equal to \ttheta\ tolerance, the peak counts as indexed, otherwise as unindexed. The tolerance can be adjusted with setting \texttt{[Main] twoThetaTolerance}.

Number of possible peaks is determined by counting calculated peaks with \ttheta\ position lower than or equal to the highest \ttheta\ position in the list of observed peaks (increased by aforementioned \ttheta\ tolerance). Currently, there is no mechanism to exclude calculated peaks with lower \ttheta\ position than that in the list of observed peaks.



\section{Cell tracker}

\texttt{CellTracker} tracks parameters and energy of calculated unit cells in a single thread. Data is stored in RAM. At the end of calculations the data is merged (in RAM) and finally written to file \texttt{tracked\_data.dat} in the current working directory.

A specialized cell tracker implementation exists for Monte Carlo simulations. It stores simulation-related data, such as temperature and maximum length and angle steps, in addition to unit cell data.

Because of large number of steps stored it is advised to use this module with caution (for a small number of simulations only). Otherwise it can consume large quantities of RAM and, when the data is written to a file, disk space.

Cell tracker will only be used if the setting \texttt{[Logging] cellTracker} is set to a value greater than zero.



\section{Data Extractor}

\texttt{DataExtractor} module selects the appropriate input data reader, which is then used to read input data file and populate list of observed peaks. This module has only one implementation available and is loaded automatically by root modules.

Input data reader is chosen based on data format defined by setting \texttt{[Main] dataFormat}. Additionally, underlying components use settings \texttt{[Main] dataFile} and \texttt{[Main] zeroShift} to determine path to the input file and zero shift of \ttheta\ positions in degrees, respectively.



\section{Energy calculator}


\subsection{EmeraldEnergy}

\texttt{EmeraldEnergy} uses a modified figure of merit function used in TOPAS-Academic V6~\cite{topas6-new}. Modified function used by MCC is shown in equation~\ref{eq:topasenergy}. $N_{unind}$ represents number of unindexed peaks, $d^2_{obs, min}$ minimum observed $d^2$, $N_{calc}$ number of calculated peaks (analogue to number of possible peaks), $N_{obs}$ number of observed peaks, $d^2_{obs, i}$ $d$-value of current observed peak, and $d^2_{calc, i}$ $d$-value of current nearest calculated peak.

\begin{equation}
  E = -10.0 / \left((1 + N_{unind}) d^2_{obs, min} \frac{N_{calc}}{N_{obs}} \sum_i (d^2_{obs, i} - d^2_{calc, i})\right)
  \label{eq:topasenergy}
\end{equation}

In comparison to Smith-Snyder figure of merit, this function not only reflects relative number and matching of calculated peaks, but number of unindexed peaks as well. However, weigths of these effects are non-adjustable. To use it, set \texttt{[Modules] energyCalculator} to \texttt{Emerald}.


\subsection{EmeraldEnergyEx}

\texttt{EmeraldEnergyEx} is an extended and more configurable version of \texttt{EmeraldEnergy}. It calculates energy according to equation~\ref{eq:topasenergyexp}. Definitions from above apply, except for unindexed factor $U$ and exponential base $B$.

\begin{equation}
  E = -10.0 / \left((1 + U N_{unind}) d^2_{obs, min} B^\frac{N_{calc}}{N_{obs}} \sum_i (d^2_{obs, i} - d^2_{calc, i})\right)
  \label{eq:topasenergyexp}
\end{equation}

Exponential base $B$ can be set using \texttt{[Energy] EmeraldExpBase} setting. Increasing the base will penalize excess calculated peaks more and yield unit cells with lower volume.

Unindexed factor $U$ can be set by \texttt{[Energy] EmeraldUnindexedFactor} setting. Increasing it above 1.0 will make the function more sensitive to unindexed peaks (relative to \texttt{EmeraldEnergy}). In contrast, setting it between 0.0 and 1.0 will make it less sensitive to unindexed peaks.

This module implementation can be used by setting \texttt{[Modules] energy\-Cal\-cu\-la\-tor} to \texttt{EmeraldEx}.


\subsection{SmithSnyderEnergy}

\texttt{SmythSnyderEnergy} calculates energy based on Smith-Snyder figure of merit $F_N$ using a simple equation~\ref{eq:smithsnyderenergy}. The higher the figure of merit, the lower the energy. It uses \texttt{SmithSnyderEvaluator} described in section~\ref{sec:smith-snyder-evaluator}.

\begin{equation}
  E = -F_N
  \label{eq:smithsnyderenergy}
\end{equation}

It can be used by setting \texttt{[Modules] energyCalculator} to \texttt{SmithSnyder}.


\subsection{SmithSnyderEnergyEx}

\texttt{SmithSnyderEnergyEx} is an experimental energy calculator made to additionally penalize cells with high number of calculated peaks compared to number of observed peaks. It uses Smith-Snyder figure of merit $F_N$ as energy basis. Energy is increased linearly for each calculated peak that does not have an observed match (determined by peak count). Energy is calculated by equation~\ref{eq:nnpossssenergy}, where $N_{poss}$ is number of possible reflections the unit cell can produce (calculated as described in section~\ref{sec:module-cellstats}), $N$ number of observed peaks, and $P$ penalty scale factor.

\begin{equation}
  E = -F_N + (N_{poss} - N) P
  \label{eq:nnpossssenergy}
\end{equation}

Penalty scale factor can be adjusted by \texttt{[Energy] SmithSynderExFactor} setting. The higher it is, the more excess peaks are penalized.

Since this energy calculator is experimental, it is not optimized. Determination of possible peaks is done twice (once in \texttt{SmithSnyderEvaluator} and once in this module), which may affect performance. The module can be used by setting \texttt{[Modules] energyCalculator} to \texttt{SmithSnyderEx}.



\section{Observed peak list}

\subsection{DynamicObservedList}

\texttt{DynamicObservedList} uses dynamic array to store observed peak data (\ttheta\ positions in degrees and intensities). It requires no special configuration. It can be used by setting \texttt{[Modules] observedList} to \texttt{Dynamic}.



\section{Peak generator}


\subsection{AllowedPeakGenerator}

\texttt{AllowedPeakGenerator} calculates peaks while filtering out symmetrically equivalent $hkl$ combinations (checking if reflection is \enquote{allowed}) , as described in reference \cite{fundamentals-pxrd}, page~414. Index generation procedure is described in reference \cite{fundamentals-pxrd}, page~406.

The module determines minimum and maximum indices automatically by calculating minimum index necessary for the peak to fall beyond minimum $d$ (while assuming the other indices are 0, since greater incides will result in even lower $d$ anyway). Determination of index bounds has to be carried out individually for each unit cell.

This module can be used by setting \texttt{[Modules] peakGenerator} to \texttt{Allowed}.



\section{Root module}


\subsection{GridSearchRootModule}

\texttt{GridSearchRootModule} controls \texttt{GridSearchControllers} performing grid search calculations in separate threads. It gives them work and collects solutions.

In grid search calculations work is divided in pre-generated work plans. Each thread receives a work plan, performs calculations in the area described by the work plan, and asks for next work plan. This is to ensure that all threads get approximately equal amount of work (otherwise some threads would finish sooner than others and leave the CPU cores idle).

To perform grid search, set \texttt{[Modules] rootController} to \texttt{"GridSearch"}.

\subsubsection*{Operating procedure}

\begin{enumerate}
  \item Work plans are generated using procedure described below.
  \item Observed peak data is extracted.
  \item Grid search controllers are started (observed peak data is copied), each in its own thread.
  \item Progress is displayed in the console until all threads finish their work.
  \item Solutions are written to a file.
  \item If cell tracking was enabled, cell tracker data from individual threads is merged in a single \texttt{CellTracker} object and then written to a file.
\end{enumerate}

\subsubsection*{Generation of work plans}

Currently, a so-called divider method is used to generate work plans. First, it calculates a divider using equation~\ref{eq:workplan-divider}. $D$ represents the calculated divider, $Z$ number of variables and $N_{plans}$ number of desired work plans. Number of variables is the total number of independent length and angle parameters of the unit cell (it depends on the crystal system of the unit cell and is, for example 1 for cubic cells and 6 for triclinic cells). Floor is a function that rounds a decimal number down to the nearest integer.

\begin{equation}
  D = \mathrm{floor}\left(\sqrt[Z]{N_{plans}}\right)
  \label{eq:workplan-divider}
\end{equation}

$N_{plans}$ can be adjusted with setting \texttt{[Parallel] workAreaDivider}. $D$ is calculated to produce number of work plans that is close to $N_{plans}$ while still keeping work division relatively simple.

Then, so-called chunks are calculated for each unit cell parameter according to equation~\ref{eq:workplan-chunk}. $C_x$ represents chunk, calculated for parameter $x$, while $x_{max}$ and $x_{min}$ represent maximum and minimum values for parameter $x$, respectively. Cell parameter limits can be adjusted using setting in group \texttt{[Cell]} (see section~\ref{sec:settings-cell}).

\begin{equation}
  C_x = \frac{x_{max} - x_{min}}{D}
  \label{eq:workplan-chunk}
\end{equation}

Finally, work plans are generated using nested loops of depth $Z$ (this is the reason divider $D$ needed to be calcualted to produce desired number of work plans). Bounds for each parameter are calculated using equations \ref{eq:workplan-planmin} and \ref{eq:workplan-planmax}. $i$ represents an integer index of the loop (iterating from 0 to $D - 1$) and $S$ grid search step for the parameter (either length or angle step).

\begin{equation}
  x_{plan,min} = x_{min} + i C_x
  \label{eq:workplan-planmin}
\end{equation}
\begin{equation}
  x_{plan,max} = x_{min} + (i + 1) C_x - S
  \label{eq:workplan-planmax}
\end{equation}



\subsection{MonteCarloRootModule}

\texttt{MonteCarloRootModule} controls \texttt{MonteCarloControllers} performing Monte Carlo simulations in separate threads. It assigns them initial unit cells and collects solutions.

Monte Carlo controllers need initial unit cells to begin simulations. Each time they need one, they ask the root controller for it. Initial unit cells are generated randomly. Each parameter is assigned a random value between its minimum and maximum limits.

Root module also keeps track of number of simulations. If number of simulations is about to exceed the maximum number, no more initial cells will be generated and threaded controllers will not be able to begin new simulations. Number of maximum simulations can be adjusted with setting \texttt{[MonteCarlo] nMaxSimulations}.

To perform Monte Carlo simulations, set setting \texttt{[Modules] rootController} to \texttt{"MonteCarlo"}.

\subsubsection*{Operating procedure}

\begin{enumerate}
  \item Observed peak data is extracted.
  \item Monte Carlo controllers are started (observed peak data is copied), each in its own thread.
  \item Progress is displayed in the console until all threads finish their work.
  \item Solutions are written to a file.
  \item If cell tracking was enabled, cell tracker data from individual threads is merged in a single \texttt{CellTracker} object and then written to a file.
\end{enumerate}



\section{Solution storage}


\subsection{DynamicSolutionStorage}

\texttt{DynamicSolutionStorage} uses dynamic array to store solutions, which consist of unit cell parameters, crystal system, energy and cell statistics (number of indexed and unindexed peaks, number of calculated and observed peaks). It is the only available module for solution storage and is loaded automatically by root modules.

The most important setting for this module is \texttt{[Main] nStoredCells}. It determines maximum number of solutions stored in the module. If more solutions get submitted, their energies are compared to the worst solution already in the storage. New solution replaces the worst solution in the storage, if it has lower energy.

It is also checked whether submitted solution is very similar to an already present solution. In this case new solution replaces the old one if its energy is lower. This behaviour is controlled by settings \texttt{lengthTolerance}, \texttt{angleTolerance} and \texttt{energyTolerance} in group \texttt{[Solutions]}. They determine the maximum absolute difference between length parameters, angle parameters and energies of new and old cells, respectively, for them to be treated as similar. Such mechanism reduces cluttering of solution storage with nearly identical solutions, while other unique solutions with higher energy get pushed out.



\section{Step optimizer}


\subsection{LinearStepOptimizer}

\texttt{LinearStepOptimizer} uses a linear function to optimize maximum steps in Monte Carlo simulations. Its only configurable parameter is setting \texttt{[MonteCarlo] ac\-cept\-ed\-Step\-Ratio}, representing ideal ratio between accepted and total Monte Carlo steps. Other constants used are currently non-adjustable.

Step optimization function is shown in equation~\ref{eq:linearstepopt}. $\Delta S$ represents change of given maximum step parameter, $C$ scaling constant for step optimization, $N_{accepted}$ and $N_{total}$ number of accepted and total simulation steps, respectively, and $R_{ideal}$ ideal ratio $\frac{N_{accepted}}{N_{total}}$.

\begin{equation}
  \Delta S = C \left(\frac{N_{accepted}}{N_{total}} - R_{ideal} \right)
  \label{eq:linearstepopt}
\end{equation}

For stability reasons, change $\Delta S$ is limited by non-configurable constant. Maximum step itself has maximum and minimum limits as well.

This module can be used by setting \texttt{[Modules] stepOptimizer} to \texttt{"Linear"}.


\subsection{LogarithmicStepOptimizer}

\texttt{LogarithmicStepOptimizer} uses a logarithm function to optimize maximum steps in Monte Carlo simulations. As in previous case, its only setting is \texttt{[MonteCarlo] ac\-cept\-ed\-Step\-Ratio}, representing ideal ratio between accepted and total Monte Carlo steps.

Step optimization functions for two different cases are shown in equations \ref{eq:logstepopt-a} and \ref{eq:logstepopt-b}. $D$ represents difference between real and ideal ratio of accepted and total steps, $N_{accepted}$ and $N_{total}$ number of accepted and total simulation steps, respectively, $R_{ideal}$ ideal ratio $\frac{N_{accepted}}{N_{total}}$, $\Delta S$ change of given maximum step parameter and $C$ scaling constant for step optimization.

\begin{align}
  D = \frac{N_{accepted}}{N_{total}} - R_{ideal} \nonumber \\[3mm]
  D \geq 0:\;\Delta S = -C \ln(D) \label{eq:logstepopt-a} \\
  D < 0:\;\Delta S = C \ln(-D) \label{eq:logstepopt-b}
\end{align}

For stability reasons, change $\Delta S$ is limited by non-configurable constant. Maximum step itself has maximum and minimum limits as well.

To use this module, set \texttt{[Modules] stepOptimizer} to \texttt{"Logarithmic"}.



\section{Temperature controller}


\subsection{AutoCosineTemperatureController}

\texttt{AutoCosineTemperatureController} uses cosine function to calculate temperature. It calculates its parameters from human-readable \texttt{auto} temperature parameters.

Equations for calculation of function parameters are shown in equation list~\ref{eq:autocosine-params}. Parameter $A$ adjusts amplitude, $S$ horizontal scale (stretch or shrink) and $V$ vertical shift. They are calculated from \texttt{auto} settings in group \texttt{[Temperature]}. $T_{min}$ and $T_{max}$ represent minimum and maximum temperature limits, respectively, $N_{periods}$ number of desired cosine function periods in non-constant step area, $N_{steps}$ total number of simulation steps, and $N_{const,b}$ and $N_{const,e}$ number of begining and end constant temperature steps, respectively.

\begin{align}
  \begin{split}
    A &= \left|\frac{T_{min} - T_{max}}{2}\right| \\
    S &= 2\pi\,\frac{N_{periods}}{N_{steps} - (N_{const,b} + N_{const,e})} \\
    V &= T_{min} + A
  \end{split}
  \label{eq:autocosine-params}
\end{align}

For first $N_{const,b}$ steps of simulation, temperature is $T_{max}$. For last $N_{const,e}$ steps of simulation, temperature is $T_{min}$. Otherwise temperature is calculated using equation~\ref{eq:autocosine-t}, where $x$ is current step number (not counting beginning constant temperature steps).

\begin{equation}
  T = A \cos(x S) + V
  \label{eq:autocosine-t}
\end{equation}

To use this module, set \texttt{[Modules] temperatureController} to \texttt{"AutoCosine"}.


\subsection{AutoLinearTemperatureController}

\texttt{AutoLinearTemperatureController} uses linear function to calculate a descending temperature profile. It calculates its parameters from human-readable \texttt{auto} temperature parameters.

Equations for calculation of function parameters are shown in equation list~\ref{eq:autolinear-params}. $S$ and $I$ are slope and intercept of linear function, respectively. Other symbols represent \texttt{auto} settings of group \texttt{[Temperature]}: $T_{min}$ and $T_{max}$ are minimum and maximum temperature limits, respectively, $N_{steps}$ total number of simulation steps, and $N_{const,b}$ and $N_{const,e}$ number of begining and end constant temperature steps, respectively.

\begin{align}
  \begin{split}
    S &= \frac{T_{min} - T_{max}}{N_{steps} - (N_{const,b} + N_{const,e})} \\
    I &= T_{max} - S N_{const,b}
  \end{split}
  \label{eq:autolinear-params}
\end{align}

For first $N_{const,b}$ steps of simulation, temperature is $T_{max}$. For last $N_{const,e}$ steps of simulation, temperature is $T_{min}$. Otherwise temperature is calculated using equation~\ref{eq:autolinear-t}, where $x$ is current step number.

\begin{equation}
  T = S x + I
  \label{eq:autolinear-t}
\end{equation}

To use this module, set \texttt{[Modules] temperatureController} to \texttt{"AutoLinear"}.


\subsection{LinearTemperatureController}

\texttt{LinearTemperatureController} is a manual version of temperature controller using linear function. Function parameters are specified by user directly using settings \texttt{linearIntercept}, \texttt{linearSlope} and \texttt{minimumTemperature} in context of group \texttt{[Temperature]}.

Temperature is calculated using equation~\ref{eq:lineartemperature}, where $S$ is slope, $x$ current simulation step number and $I$ intercept of linear function.

\begin{equation}
  T = S x + I
  \label{eq:lineartemperature}
\end{equation}

This module can be used by setting \texttt{[Modules] temperatureController} to \texttt{"Linear"}.



\section{Threaded controller}


\subsection{GridSearchController}

\texttt{GridSearchController} performs a grid search calculation within limits defined by work plan (received from \texttt{GridSearchRootModule}). Calculations are executed in a separate thread. This module is loaded automatically by \texttt{GridSearchRootModule}.

Grid search process can be configured by \texttt{lengthStep} and \texttt{angleStep} settings in group \texttt{[GridSearch]}. These settings determine by how much unit cell parameters will be incremented while searching the area defined in work plan. Lower steps offer higher chances of finding appropriate unit cell, but can considerably slow down the calculation process and increase amount of data when cell tracking is enabled (controlled by setting \texttt{[Logging] cellTracker}). However, setting steps too high will prove fatal, because energy minima are extremely narrow in the indexing problem.

\subsubsection*{Operating procedure}

\begin{enumerate}
  \item Initialization status of child modules and observed peak list are checked. Thread terminates in case of errors.
  \item Program enters main calculation loop:
  \begin{enumerate}
    \item Interrupt flag is checked. If user signalled interrupt, thread finishes its work.
    \item Work plan is retrieved from the root module. If retrieval fails, thread finishes its work.
    \item Based on crystal system, program enters a nested loop which constructs unit cells for every combination of parameters inside the bounds of work plan, and calculates their energies (which requires calculating, sorting and removing duplicate peaks). Solutions with low enough energies are submitted to the root module. If cell tracking is enabled, all unit cells are tracked after energy calculation.
  \end{enumerate}
\end{enumerate}


\subsection{MonteCarloController}

\texttt{MonteCarloController} performs a Monte Carlo simulation within limits defined by settings. Calculations are executed in a separate thread. This module is loaded automatically by \texttt{MonteCarloRootModule}.

\subsubsection*{Operating procedure}

\begin{enumerate}
  \item Initialization status of child modules and observed peak list are checked. Thread terminates in case of errors.
  \item Program enters main calculation loop:
  \begin{enumerate}
    \item Interrupt flag is checked. If user signalled interrupt, thread finishes its work.
    \item Initial unit cell is retrieved from the root module. If retrieval fails, thread terminates.
    \item First energy is calculated using the usual procedure: calculating peaks, sorting them, removing duplicates and calculating energy.
    \item Program begins Monte Carlo simulation.
  \end{enumerate}
\end{enumerate}

\subsubsection*{Monte Carlo simulation procedure}

\begin{enumerate}
  \item Perform one simulation step on each independent length parameter (depends on crystal system). Repeat according to setting \texttt{[MonteCarlo] nLength\-Step\-Cycles}.
  \item Perform one simulation step on each independent angle parameter (depends on crystal system). Repeat according to setting \texttt{[MonteCarlo] nAngle\-Step\-Cycles}.
  \item Check if enough steps have done to perform step optimization (checked against non-adjustable constant). If yes, perform step optimization.
  \item End simulation if maximum number of steps was reached.
\end{enumerate}

\subsubsection*{Monte Carlo step procedure}

\begin{enumerate}
  \item Copy of a parameter, chosen for Monte Carlo step, is made.
  \item Parameter is modified according to equation~\ref{eq:montecarlo-step}, where $x$ represents the parameter, $\xi$ random number between $-1$ and $1$, and $S_{max}$ maximum step (either for length or angle parameters).
  \begin{equation}
    x_{new} = x_{old} + \xi S_{max}
    \label{eq:montecarlo-step}
  \end{equation}
  \item If new parameter went outside its limits, it is set back to the nearest limit.
  \item Energy of the new unit cell is calculated. Energy difference between old and new cell is calculated. If energy of the new cell is lower, step is accepted. Otherwise step is accepted if the condition in equation~\ref{eq:montecarlo-evaluation} is true. $\chi$ is a random number between $0$ and $1$, $\Delta E$ is difference between energy of new and old unit cell, and $T$ is current temperature of the simulation.
  \begin{equation}
    \chi < e^{\frac{-\Delta E}{T}}
    \label{eq:montecarlo-evaluation}
  \end{equation}
  \item Total step counter is increased. If step was accepted, accepted step counter is increased as well.
  \item If step was rejected, old parameter value is restored from the copy.
\end{enumerate}