\chapter{Program architecture}



\section{Modules}

With a few exceptions, MCC is organized in a hierarchy of modules. There may be more available module implementations for the same task and the user chooses which one to use. A graphical overview of module hierarchy is presented in figure~\ref{fig:module-hierarchy}.

\begin{figure}[h]
  \centering
  \includegraphics[height=0.45\textheight]{img/module-hierarchy.pdf}
  \caption{Module hierarchy in MCC. Each box represents a module. Arrows represent parent-child relationship between modules and point from parent to child. Dashed boxes represent optional modules, which may only be present in certain implementations.}
  \label{fig:module-hierarchy}
\end{figure}

Modules listed in the figure have generic names and each represents 
certain functionality, explained in table~\ref{tab:module-roles}. When MCC is ran, practical module implementations, fitting particular purpose, take their places. A typical example is Root module, having grid search and Monte Carlo implementations, one of which is chosen based on the type of simulation user wants to run. Module implementations are discussed in chapter~\ref{ch:module-reference}. Settings regarding module choice are listed in section~\ref{sec:settings-modules}.

\begin{table}[p]
  \centering
  \caption{Descriptions of module roles in MCC.}
  \begin{tabular}{ l p{0.6\textwidth} }
    \textbf{Generic module name} & \textbf{Module role} \\
    \hline
    Calculated peak list & Contains a list of peaks, calculated by peak generator module, with additional data like Miller indices and d-values. \\
    Cell evaluator & Calculates figure of merit for given cell by comparing lists of calculated and observed peaks. \\
    Cell stats & Calculates cell statistics like number of indexed and unindexed peaks, and number of possible peaks in observed \si{2\Theta} interval. \\
    Cell tracker & Tracks unit cell parameters during a simulation for debugging purposes. In case of Monte Carlo simulation, simulation parameters like temperature and maximum steps are also tracked. \\
    Data extractor & Selects appropriate data reader based on settings and uses it to populate list of observed peaks. \\
    Energy calculator & Calculates energy, used to rank unit cells, by comparing lists of calculated and observed peaks. Can use a child Cell evaluator module in the process. \\
    Observed peak list & Contains a list of observed peaks, read from an input data file, along with peak intensities. \\
    Peak generator & Generates all possible peaks given unit cell could produce in \si{2\Theta} interval of observed peaks. \\
    Root module & Controls all threads by giving them work and collecting solutions. \\
    Solution storage & Stores best unit cells found by threaded controllers. \\
    Step optimizer & Optimizes maximum length and angle steps in Monte Carlo simulations. \\
    Temperature controller & Controls temperature in Monte Carlo simulations. \\
    Threaded controller & Performs a single Monte Carlo simulation or grid search on a certain slice of search area. Meant to run on one CPU core for calculations. \\
  \end{tabular}
  \label{tab:module-roles}
\end{table}



\section{Program flow}

MCC operates in well-defined phases. Program flow can be broken down to individual steps:

\begin{enumerate}
  \itemsep0em
  \item Basic program information like name and version is displayed.
  \item Command line parameters are parsed. If errors are encountered, the program exits.
  \item Based on command line parameters, decision about what to do is made.
    \begin{itemize}
      \item If request to write default configuration file was received, default configuration file is written and the program exits.
      \item Otherwise program continues.
    \end{itemize}
  \item Configuration file is read:
    \begin{enumerate}
      \item File is opened and setting-value pairs are read, parsed values overriding defaults. If errors were encountered during the reading, information about them is displayed and program is terminated.
      \item Setting evaluation is performed. Settings are checked for validity by taking multiple settings into account. As in the previous case, program is terminated in case of errors.
    \end{enumerate}
  \item Appropriate root module is loaded depending on the settings. Observed data is read and shared with newly created threaded controller objects (their child modules are loaded according to the settings). Monte Carlo simulations or grid search processes are ran. Each threaded controller performs one simulation at a time, while threaded controllers run in parallel (each in its own thread).
  \item When simulations are finished, found solutions are printed to the screen. Solutions are also written to file \texttt{solutions.dat} in the current working directory. If cell tracking was enabled, tracked data is written to a file as well.
\end{enumerate}



\section{Command line parameters}

Command line parameters are arguments that can be passed to a program when it is ran. This is usually done through command line interface, where arguments follow the command that executes the program. For example, if \texttt{mcc.exe} executes MCC, command line parameter, that will make MCC use alternate configuration file \texttt{another\_file.conf} instead of the default one, can be introduced like this:
\begin{lstlisting}
  $ mcc.exe another_file.conf
\end{lstlisting}

Command line parameters are separated from the command and from each other with a space character. If multi-word argument is required, it should be enclosed in quotes. List of command line parameters available in MCC is presented in table~\ref{tab:command-line-parameters}.

\begin{table}[h]
  \centering
  \caption{List of command line parameters supported by MCC.}
  \begin{tabular}{ l p{0.6\textwidth} }
    \textbf{Argument} & \textbf{Effect} \\
    \texttt{-c}       & Write default configuration file to \texttt{mcc.conf} in the current working directory, and do not perform calculations. If the file already exists, no action will be taken. \\
    file name         & Use provided path to the alternate configuration file instead of the default one.
  \end{tabular}
  \label{tab:command-line-parameters}
\end{table}



\section{Settings}
\label{sec:settings}

Modules and other parts of MCC may require parameters to operate properly. Additionally, information about which modules to use has to be known in the first place. This information is provided by \textbf{global settings} object. Only one such object exists during execution of the program, and all modules have read access to it. It contains all configurable settings, their values set to default when MCC is ran. When the configuration file is read, setting values are parsed and assigned to the globals settings object, overriding default values.


\subsection{Configuration file format}

Settings are organized into \textbf{groups}. Each setting has a \textbf{name} and only one \textbf{value}. Although there is support for multi-word group (enclosed in square brackets) and setting names (enclosed in quotation marks \texttt{"}), in practice names do not contain whitespace.

\textbf{Groups} (usually notated in square brackets like this: \texttt{[GroupName]}) provide context for settings. Settings with a common purpose often belong to the same group. It is possible for multiple settings with exactly the same name to exist in different groups. Group names are in \texttt{UpperCamelCase}.

\textbf{Setting names} connect setting values to their particular purposes. They are usually informative enough for the user to figure out their meaning by the names alone. They are written in \texttt{lowerCamelCase}.

\textbf{Setting values} are assigned to settings and can be either integers, floating point numbers or strings. The type is determined individually for each setting. Decimal separator is decimal point (\texttt{.}), not decimal comma (\texttt{,}).

Configuration file can also contain \textbf{comments}. Their purpose is to provide additional information that setting names by themselves do not, and for users to be able to take configuration-related notes. Comments always begin with character~\texttt{\#} and last until the end of the line. There is no notation for multi-line comments. Comments can begin at an arbitrary column in the line.

There should only be one group name (group declaration) per line, and it should be the only thing in that line:
\begin{lstlisting}
  [GroupName]   # Comments are allowed, however
\end{lstlisting}

Similarly, there should only be one setting-value pair per line. Additionally, it must be in a single line:
\begin{lstlisting}
  mySetting = 1337.404
\end{lstlisting}

Each setting always belongs to a group, which should be declared before the setting-value pair. All subsequent settings belong to that group until a new one is declared.


\subsection{Reading procedure}

MCC looks for either default configuration file \texttt{mcc.conf} or alternate configuration file, if it was provided with command line parameter, in the current working directory. It tries to open the file for reading. If that fails, an error message is printed and the program terminates. Same goes for any other input/output error that could occur. Otherwise the file is read line by line and interpreted by following procedure:

\begin{enumerate}
  \itemsep0em
  \item A check is performed whether the line is empty or a comment line. It is skipped in any of these cases.
  \item Line is checked for group declaration. If group name is found, it is checked for validity (invalid group name terminates reading) and the line is also checked for invalid text after the declaration. Otherwise interpretation continues.
  \item It is assumed that the line contains a setting-value pair. First, setting name is captured. Second, the program looks for assignment operator (\texttt{=}) following the setting name (its absence terminates reading). Finally, setting value is captured and the line is checked for invalid text after the value.
  \item Captured setting-value pair is evaluated in context of the last group declared. It is rejected if either of group or setting names are invalid, the value fails to parse (convert from text to value understood by computer), or parsed value is not within required range. If the evaluation is successful, parsed value overwrites the default value of the setting. Otherwise setting-value pair is ignored and user is notified with a message in the console.
\end{enumerate}

MCC will provide insight into errors it encounters, usually precisely reporting the line number, group or setting names associated, or text that it was not able to parse. Messages will be printed to the console.



\section{Input data}
\label{sec:input-data}

MCC is engineered to be able to read input data in multiple formats. Format to be used is specified by user with \texttt{[Main] dataFormat} setting.

Input data should be supplied in a text file with an arbitrary name (extension does not matter, since format is determined by a setting). Name of the input file should be set using \texttt{[Main] dataFile} setting.

Currently, only one format is supported. See the next section for its description.


\subsection{XY data format}

XY format consists of one or two numbers in a single line, separated by whitespace. The first number represents \si{2\Theta} position of a peak in degrees, and the second number represents its intensity. This data format can be used by setting \texttt{[Main] dataFormat} to \texttt{"xy"}.

If \si{2\Theta} position is negative, the peak is ignored (useful for quickly excluding specific peaks). Zero shift (determined by \texttt{[Main] zeroShift} setting) is added to the \si{2\Theta} position immediately after the value is read. Intensity is optional and is presently not used in calculations. If no intensity is found, the default value of 0.0 is used. Leading and trailing whitespace is ignored.

An example of input file in XY format is shown in listing~\ref{lst:input-xy}. The first column contains \si{2\Theta} positions of peaks and the second column contains their intensities. Columns are separated with spaces so that numbers are right-aligned for human readability (for MCC one space would be enough to separate the numbers). Similarly, some numbers in the first column are preceeded with a space, which is also ignored by MCC.

\begin{lstlisting}[caption={Example of data file in XY format (peaks of platinum, \si{CuK\alpha} radiation).},label={lst:input-xy}]
  39.7634  100
  46.2427   53
  67.4541   31
  81.2861   33
  85.7121   12
 103.5078    6
 117.7112   22
 122.8070   20
 148.2614   29 
\end{lstlisting}