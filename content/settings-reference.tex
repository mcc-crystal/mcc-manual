\chapter{Settings reference}
\label{ch:settings-reference}

This chapter lists all the settings available for customization in the configuration file. Settings are ordered by groups in alphabetical order.


\section{[Cell]}
\label{sec:settings-cell}

Group \texttt{[Cell]} contains limits for the search of unit cell. List of settings in this group is presented in table~\ref{tab:group-cell}.

\begin{table}[h]
  \centering
  \caption{List of settings in group \texttt{[Cell]}.}
  \label{tab:group-cell}
  \begin{tabular}{ l l c r }
    \textbf{Setting name} & \textbf{Value type} & \textbf{Unit} & \textbf{Default value} \\
    \hline
    \texttt{crystalSystem}   & String   &              & \texttt{orthorhombic} \\
    \texttt{maxA}            & Float    & \AA          & 10.0 \\
    \texttt{maxAlpha}        & Float    & \si{\degree} & 120.0 \\
    \texttt{maxAngle}        & Float    & \si{\degree} & 0.0 \\
    \texttt{maxB}            & Float    & \AA          & 10.0 \\
    \texttt{maxBeta}         & Float    & \si{\degree} & 120.0 \\
    \texttt{maxC}            & Float    & \AA          & 10.0 \\
    \texttt{maxGamma}        & Float    & \si{\degree} & 120.0 \\
    \texttt{maxLength}       & Float    & \AA          & 0.0 \\
    \texttt{minA}            & Float    & \AA          & 2.0 \\
    \texttt{minAlpha}        & Float    & \si{\degree} & 90.0 \\
    \texttt{minAngle}        & Float    & \si{\degree} & 0.0 \\
    \texttt{minB}            & Float    & \AA          & 2.0 \\
    \texttt{minBeta}         & Float    & \si{\degree} & 90.0 \\
    \texttt{minC}            & Float    & \AA          & 2.0 \\
    \texttt{minGamma}        & Float    & \si{\degree} & 90.0 \\
    \texttt{minLength}       & Float    & \AA          & 0.0 \\
  \end{tabular}
\end{table}

Cell parameter limits can be set either individually or all at once. Settings \texttt{minLength}, \texttt{maxLength}, \texttt{minAngle} and \texttt{maxAngle} will set minimum or maximum values for all unit cell length parameters ($a$, $b$ and $c$) or angle parameters ($\alpha$, $\beta$ and $\gamma$) if their value is greater than zero. This will override individual parameter settings (warning will be displayed in the console). As an example, setting \texttt{maxLength} with value of \num{40.4} will cause \texttt{maxA}, \texttt{maxB} and \texttt{maxC} to be set to \num{40.4}.

Unit cell length limits can be set individually with settings \texttt{minA}, \texttt{minB}, \texttt{minC}, \texttt{maxA}, \texttt{maxB} and \texttt{maxC}. Angle limits can be set by \texttt{minAlpha}, \texttt{minBeta}, \texttt{minGamma}, \texttt{maxAlpha}, \texttt{maxBeta} and \texttt{maxGamma}. Values of these settings may be overriden (refer to previous paragraph).

All unit cell length parameter limits must be greater than zero. Angle parameter limits must be within interval from \SI{80.0}{\degree} to \SI{180.0}{\degree}. Each minimum bound must not be greater than the maximum one.

\texttt{crystalSystem} can be one of the following options: \texttt{cubic}, \texttt{hexagonal}, \texttt{te\-tra\-go\-nal}, \texttt{orthorhombic}, \texttt{monoclinic} or \texttt{triclinic}.



\section{[Energy]}

Group \texttt{[Energy]} contains parameters for calculation of energy, which can be used to make cell ranking more punishing or forgiving towards unindexed peaks, too much calculated peaks and so on. List of settings in this group is presented in table~\ref{tab:group-energy}.

\begin{table}[h]
  \centering
  \caption{List of settings in group \texttt{[Energy]}.}
  \label{tab:group-energy}
  \begin{tabular}{ l l c r }
    \textbf{Setting name} & \textbf{Value type} & \textbf{Unit} & \textbf{Default value} \\
    \hline
    \texttt{EmeraldExpBase}         & Float    &       & 1.7 \\
    \texttt{EmeraldUnindexedFactor} & Float    &       & 1.0 \\
    \texttt{SmithSnyderExFactor}    & Float    &       & 0.005 \\
  \end{tabular}
\end{table}

\texttt{SmithSnyderExFactor} is a parameter used by module \texttt{SmithSnyderEx}. It must be greater than zero.

\texttt{EmeraldExpBase} and \texttt{EmeraldUnindexedFactor} are parameters used by module \texttt{EmeraldEnergyEx}. They must be greater than zero.



\section{[GridSearch]}

Group \texttt{[GridSearch]} contains parameters for directing grid search. List of settings in this group is presented in table~\ref{tab:group-gridsearch}.

\begin{table}[h]
  \centering
  \caption{List of settings in group \texttt{[GridSearch]}.}
  \label{tab:group-gridsearch}
  \begin{tabular}{ l l c r }
    \textbf{Setting name} & \textbf{Value type} & \textbf{Unit} & \textbf{Default value} \\
    \hline
    lengthStep       & Float    & \AA          & 0.01 \\
    angleStep        & Float    & \si{\degree}  & 0.10 \\
  \end{tabular}
\end{table}

\texttt{lengthStep} and \texttt{angleStep} are grid search steps used for unit cell length parameters and angle parameters, respectively. They must  be greater than zero.



\section{[Logging]}

Group \texttt{[Logging]} contains settings related to logging data for testing or debugging purposes. List of settings in this group is presented in table~\ref{tab:group-logging}.

\begin{table}[h]
  \centering
  \caption{List of settings in group \texttt{[Logging]}.}
  \label{tab:group-logging}
  \begin{tabular}{ l l c r }
    \textbf{Setting name} & \textbf{Value type} & \textbf{Unit} & \textbf{Default value} \\
    \hline
    cellTracker     & Integer    &   & 0 \\
  \end{tabular}
\end{table}

Setting \texttt{cellTracker} enables logging of grid search or Monte Carlo data for every accepted step of the simulation if its value is greater than zero. \textbf{Warning:} use only with a single simulation or small number of short simulations. Data is stored in RAM for each thread separately and is later merged in an inefficient manner before being written to a file, which can become large in case of great number of total steps.



\section{[Main]}

Group \texttt{[Main]} contains general settings related to execution of the program. List of settings in this group is presented in table~\ref{tab:group-main}.

\begin{table}[h]
  \centering
  \caption{List of settings in group \texttt{[Main]}.}
  \label{tab:group-main}
  \begin{tabular}{ l l c r }
    \textbf{Setting name} & \textbf{Value type} & \textbf{Unit} & \textbf{Default value} \\
    \hline
    \texttt{dataFile}                & String    &                      & \texttt{""} \\
    \texttt{dataFormat}              & String    &                      & \texttt{"xy"} \\
    \texttt{nStoredCells}            & Integer   &                      & 5 \\
    \texttt{twoThetaTolerance}       & Float     & \si{\degree 2\Theta} & 0.10 \\
    \texttt{wavelength}              & Float     & \AA                  & 1.5406 \\
    \texttt{zeroShift}               & Float     & \si{\degree 2\Theta} & 0.0 \\
  \end{tabular}
\end{table}

\texttt{dataFile} represents path to input data file (can be either absolute or relative, or just the name of the file if the file resides in the current working directory). \texttt{dataFormat} specifies format of that file. See section~\ref{sec:input-data} for more information about input files.

\texttt{nStoredCells} controls the amount of best unit cell solutions that MCC remembers and prints after the simulations are complete. The integer should be greater than zero.

\texttt{twoThetaTolerance} is the tolerance, which determines whether an observed peak is considered indexed by a calculated peak. In order for the peak to be considered indexed, absolute difference between calculated and observed peak positions must be less or equal than \texttt{twoThetaTolerance}. It should be greater than zero.

\texttt{wavelength} determines the wavelength of X-ray radiation used to interconvert d-values and \si{2\Theta} positions. It should be set to the wavelength used in the powder diffraction experiment, from which input data originate.

\texttt{zeroShift} is a value that is added to all \si{2\Theta} positions read from the input data file. It can be any floating point number.



\section{[Modules]}
\label{sec:settings-modules}

Settings in group \texttt{[Modules]} determine which module implementations are used. List of settings in this group is presented in table~\ref{tab:group-modules}.

\begin{table}[h]
  \centering
  \caption{List of settings in group \texttt{[Modules]}.}
  \label{tab:group-modules}
  \begin{tabular}{ l l c r }
    \textbf{Setting name} & \textbf{Value type} & \textbf{Unit} & \textbf{Default value} \\
    \hline
    \texttt{calculatedList}          & String   &     & \texttt{"Dynamic"} \\
    \texttt{energyCalculator}        & String   &     & \texttt{"EmeraldEx"} \\
    \texttt{observedList}            & String   &     & \texttt{"Dynamic"} \\
    \texttt{peakGenerator}           & String   &     & \texttt{"Allowed"} \\
    \texttt{rootController}          & String   &     & \texttt{"MonteCarlo"} \\
    \texttt{stepOptimizer}           & String   &     & \texttt{"Logarithmic"} \\
    \texttt{temperatureController}   & String   &     & \texttt{"AutoCosine"} \\
  \end{tabular}
\end{table}

All settings listed here correspond to module roles explained in table~\ref{tab:module-roles}. For list of module implementations that can be used, see chapter~\ref{ch:module-reference}.



\section{[MonteCarlo]}

Group \texttt{[MonteCarlo]} contains settings related to Monte Carlo simulations. List of settings in this group is presented in table~\ref{tab:group-montecarlo}.

\begin{table}[h]
  \centering
  \caption{List of settings in group \texttt{[MonteCarlo]}.}
  \label{tab:group-montecarlo}
  \begin{tabular}{ l l c r }
    \textbf{Setting name} & \textbf{Value type} & \textbf{Unit} & \textbf{Default value} \\
    \hline
    \texttt{acceptedStepRatio}   & Float     &     & 0.50 \\
    \texttt{nAngleStepCycles}    & Integer   &     & 3 \\
    \texttt{nLengthStepCycles}   & Integer   &     & 3 \\
    \texttt{nMaxSimulations}     & Integer   &     & 20 \\
    \texttt{nMaxSteps}           & Integer   &     & \num{100000} \\
  \end{tabular}
\end{table}

\texttt{acceptedStepRatio} is the ideal ratio of accepted steps to total steps of a Monte Carlo simulation, that Monte Carlo controller is trying to achieve. The value must be greater than zero.

\texttt{nAngleStepCycles} is the number of times angle steps on all relevant angle parameters of the unit cell are performed in a row (before performing length steps). Similarly, \texttt{nLengthStepCycles} controls how many times all relevant unit cell length parameters are changed before steps are performed on angle parameters again. Both integers must be greater than zero.

\texttt{nMaxSimulations} determines the maximum number of Monte Carlo simulations performed. When this number is exceeded, new simulations will 
no longer be assigned to Monte Carlo threaded controllers. The value must be greater than zero.

\texttt{nMaxSteps} determines the maximum number of steps performed in Monte Carlo simulations. If this number is exceeded, the simulation is stopped. Setting value must be greater than zero.



\section{[Parallel]}

Group \texttt{[Parallel]} contains settings related to parallel computing. List of settings in this group is presented in table~\ref{tab:group-parallel}.

\begin{table}[h]
  \centering
  \caption{List of settings in group \texttt{[Parallel]}.}
  \label{tab:group-parallel}
  \begin{tabular}{ l l c r }
    \textbf{Setting name} & \textbf{Value type} & \textbf{Unit} & \textbf{Default value} \\
    \hline
    \texttt{nThreads}           & Integer   &     & 1 \\
    \texttt{workAreaDivider}    & Integer   &     & 1000 \\
  \end{tabular}
\end{table}

\texttt{nThreads} is a greater-than-zero integer determining the number of simulations ran concurrently. For greatest performance, it should be set to number of CPU cores available to the operating system. The user can also decide to use lower amount of cores to leave some available to other processes or to stress (and heat) CPU less.



\section{[PeakGeneration]}

Group \texttt{[PeakGeneration]} contains settings related to calculation of all peaks of tested unit cell in certain \ttheta\ interval. List of settings in this group is presented in table~\ref{tab:group-peakgeneration}.

\begin{table}[h]
  \centering
  \caption{List of settings in group \texttt{[PeakGeneration]}.}
  \label{tab:group-peakgeneration}
  \begin{tabular}{ l l c r }
    \textbf{Setting name} & \textbf{Value type} & \textbf{Unit} & \textbf{Default value} \\
    \hline
    \texttt{hMax}            & Integer    &     &  7 \\
    \texttt{hMin}            & Integer    &     & -7 \\
    \texttt{kMax}            & Integer    &     &  7 \\
    \texttt{kMin}            & Integer    &     & -7 \\
    \texttt{lMax}            & Integer    &     &  7 \\
    \texttt{lMin}            & Integer    &     & -7 \\
  \end{tabular}
\end{table}

Settings in this group are \textbf{obsolete}, since \texttt{AllowedPeakGenerator} now uses an automatic method of determining boundary Miller indices. These settings could be deleted in upcoming versions of MCC.



\section{[Solutions]}

Group \texttt{[Solutions]} contains settings related to determining whether two solutions are to be treated as equivalent. List of settings in this group is presented in table~\ref{tab:group-solutions}.

\begin{table}[h]
  \centering
  \caption{List of settings in group \texttt{[Solution]}.}
  \label{tab:group-solutions}
  \begin{tabular}{ l l c r }
    \textbf{Setting name} & \textbf{Value type} & \textbf{Unit} & \textbf{Default value} \\
    \hline
    \texttt{angleTolerance}         & Float    & \si{\degree} & 0.50 \\
    \texttt{energyTolerance}        & Float    &              & 2.0 \\
    \texttt{lengthTolerance}        & Float    & \AA          & 0.50 \\
  \end{tabular}
\end{table}

\texttt{angleTolerance}, \texttt{energyTolerance} and \texttt{lengthTolerance} are greater-than-zero floating point numbers used in determining whether two unit cells are to be treated as equivalent. They determine absolute maximum difference of individual angles, lengths and energy between the compared cells, respectively.



\section{[Temperature]}

Group \texttt{[Temperature]} contains parameters for temperature control in Monte Carlo simulations. List of settings in this group is presented in table~\ref{tab:group-temperature}.

\begin{table}[h]
  \centering
  \caption{List of settings in group \texttt{[Temperature]}.}
  \label{tab:group-temperature}
  \begin{tabular}{ l l c r }
    \textbf{Setting name} & \textbf{Value type} & \textbf{Unit} & \textbf{Default value} \\
    \hline
    \texttt{autoEndConstantSteps}     & Integer  &   &         0 \\
    \texttt{autoMinTemperature}       & Float    &   & $10^{-6}$ \\
    \texttt{autoMaxTemperature}       & Float    &   &       0.2 \\
    \texttt{autoPeriodsPerSimulation} & Float    &   &       5.0 \\
    \texttt{autoStartConstantSteps}   & Integer  &   &         0 \\
    \texttt{linearIntercept}          & Float    &   &       0.5 \\
    \texttt{linearSlope}              & Float    &   & $10^{-5}$ \\
    \texttt{minimumTemperature}       & Float    &   & $10^{-5}$ \\
  \end{tabular}
\end{table}

All \texttt{auto} settings are used in \texttt{TemperatureController} module implementations, that calculate temperature curve parameters automatically from more human-read\-able parameters, such as minimum (\texttt{autoMinTemperature}) and maximum temperature (\texttt{autoMaxTemperature}). If temperature function is periodic, it can also require \texttt{autoPeriodsPerSimulation} setting, which sets number of base function (for example cosine) periods that will appear during the simulation. Additionally, module implementation might support constant temperature at the beginning and the end of the simulation. Number of constant-temperature steps are determined by \texttt{auto\-Start\-Constant\-Steps} and \texttt{autoEndConstantSteps} settings, respectively.

\texttt{autoStartConstantSteps} and \texttt{autoEndConstantSteps} must be zero or greater integers. \texttt{autoMinTemperature}, \texttt{autoMaxTemperature} and \texttt{autoPeriodsPer\-Sim\-u\-la\-tion} are greater-than-zero floating point numbers.

\texttt{linearIntercept}, \texttt{linearSlope} and \texttt{minimumTemperature} are parameters used by \texttt{LinearTemperatureController} module. The first two can be arbitrary floating point numbers, while the last must be greater than zero.